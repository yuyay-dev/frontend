import { createApp } from "vue";
import App from "./App.vue";
import { setupPlugins } from "./plugins";
import { setupRouter } from "./router";
import { setupStore } from "./store";

const app = createApp(App);
// Configure plugins
setupPlugins(app);
// Configure routing
setupRouter(app);
setupStore(app);

app.mount("#app");
