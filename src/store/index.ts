import { App } from "vue";
import { createStore } from "vuex";
// import createPersistedState from "vuex-persistedstate";

const store = createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {},
  // plugins: [createPersistedState()],
});

export function setupStore(app: App<Element>) {
  app.use(store);
}

export default store;
