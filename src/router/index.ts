import { App } from "vue";
import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import { createRouterGuards } from "./routerGuards";
import { main } from "./modules/main";
import { auth } from "./modules/auth";

export const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Layout",
    redirect: "/home",
    component: () => import("@/layout/index.vue"),
    meta: {
      title: "Home",
    },
    children: [...main],
  },
  {
    path: "/login",
    name: "Login",
    // redirect: "/login",
    component: () => import("@/layout/index.vue"),
    meta: {
      title: "Login",
    },
    children: [...auth],
  },
];

const router = createRouter({
  // process.env.BASE_URL
  history: createWebHashHistory(""),
  routes,
});

export function setupRouter(app: App) {
  app.use(router);
  // Create a router guard
  createRouterGuards(router);
}
export default router;
