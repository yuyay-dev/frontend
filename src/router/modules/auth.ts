import { RouteRecordRaw } from "vue-router";

export const auth: Array<RouteRecordRaw> = [
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/auth/Login.vue"),
    meta: {
      title: "Login",
      icon: "Lock",
    },
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("@/views/auth/Register.vue"),
    meta: {
      title: "Register",
      icon: "edit",
    },
  },
];
