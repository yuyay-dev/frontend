import { RouteRecordRaw } from "vue-router";

export const main: Array<RouteRecordRaw> = [
  {
    path: "/home",
    name: "home",
    component: () => import("@/views/home.vue"),
    meta: {
      title: "Home",
      icon: "home-o",
    },
  },
  {
    path: "/featured",
    name: "featured",
    component: () => import("@/views/featured.vue"),
    meta: {
      title: "Featured",
      icon: "star-o",
    },
  },
  {
    path: "/favorites",
    name: "favorites",
    component: () => import("@/views/favorites.vue"),
    meta: {
      title: "Favorites",
      icon: "smile-o",
    },
  },
  {
    path: "/help",
    name: "help",
    component: () => import("@/views/help.vue"),
    meta: {
      title: "Help",
      icon: "info-o",
    },
  },
];
