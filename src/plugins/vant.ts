import { App } from "vue";
import "vant/lib/index.css";
import "@vant/touch-emulator"; // Desktop touch adaptation

import {
  Button,
  Card,
  Cell,
  Field,
  Form,
  Grid,
  GridItem,
  Icon,
  List,
  NavBar,
  Sidebar,
  SidebarItem,
  Skeleton,
  Sticky,
  Swipe,
  SwipeItem,
  Tabbar,
  TabbarItem,
} from "vant";

const plugins = [
  Button,
  Card,
  Cell,
  Field,
  Form,
  Grid,
  GridItem,
  Icon,
  List,
  NavBar,
  Sidebar,
  SidebarItem,
  Skeleton,
  Sticky,
  Swipe,
  SwipeItem,
  Tabbar,
  TabbarItem,
];

export const vantPlugins = {
  install: function (app: App) {
    plugins.forEach((item) => {
      app.component(item.name, item);
      // app.use(item)
    });
  },
};
