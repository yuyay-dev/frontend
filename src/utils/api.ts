import axios from "axios";

import {
  UserRegisterRq,
  UserLoginRq,
  UserProfile,
  UserRs,
} from "@/store/modules/user.d.ts";

const apiUrl = import.meta.env.VUE_APP_API_BASE || "http://localhost:1337";

export const API = axios.create({
  baseURL: apiUrl,
});

export function setJWT(jwt: string) {
  API.defaults.headers.common["Authorization"] = `Bearer ${jwt}`;
}

export function clearJWT() {
  delete API.defaults.headers.common["Authorization"];
}

export async function registerUser(
  user: UserRegisterRq
): Promise<UserRs | null> {
  try {
    const resp = await API.post("/auth/local/register", user);
    return resp.data as UserRs;
  } catch (error) {
    return null;
  }
}

export async function loginUser(user: UserLoginRq): Promise<UserRs | null> {
  try {
    const resp = await API.post("/auth/local", user);
    return resp.data as UserRs;
  } catch (error) {
    return null;
  }
}

export async function updateUser(): Promise<UserProfile | null> {
  try {
    const resp = await API.get("/users/me");
    return resp.data as UserProfile;
  } catch (error) {
    return null;
  }
}
