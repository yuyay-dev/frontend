# Menu QR

## Vite

[Vite](https://vitejs.dev/) (palabra francesa para "rápido", pronunciada / vit /) es una herramienta de construcción que tiene como objetivo proporcionar una experiencia de desarrollo más rápida y ágil para proyectos web modernos.

### Instalación

```bash
yarn create @vitejs/app frontend --template vue-ts
```

### Scripts

```javascript
    "dev": "vite", // start dev server
    "build": "vite build", // build for production
    "serve": "vite preview" // locally preview production build
```

## Vant

[Vant](https://vant-contrib.gitee.io/vant) es un kit de interfaz de usuario móvil para vue.js 2 y vue.js 3 que simplifica la creación de componentes de interfaz de usuario atractivos y amigables para dispositivos móviles.

### Instalar Vant 3 para proyectos con Vue 3

```bash
yarn add vant@next -S
```

## Router

```bash
yarn add vue-router@next -S
```

## Axios

```bash
yarn add axios -S
```

## Vuex

```bash
yarn add vuex@next vuex-module-decorators -S
```
