import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@": resolve(__dirname, "src"),
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        modifyVars: {},
        javascriptEnabled: true,
      },
      scss: {
        additionalData: `@import "src/styles/vw-rem/_util.scss";
                        @import "src/styles/vw-rem/_border.scss";
                        @import "src/styles/func.scss";`,
      },
    },
  },
  base: "./",
  server: {
    host: "localhost",
    port: 8080,
    open: true,
  },
});
